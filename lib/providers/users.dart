import 'package:flutter/material.dart';

import '../models/user.dart';

class Users with ChangeNotifier{

  List<User> _users;

  List<User> get users {
    return [..._users];
  }

}