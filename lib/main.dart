import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';
 
import './screens/home/home.dart';
import './screens/public/login.dart';
import './screens/public/check_phone.dart';
import './screens/public/password_recovery.dart';
import './screens/profile/create_profile.dart';
import './screens/profile/update_profile.dart';
import './screens/profile/public_profile.dart';
import './screens/search/search.dart';
import './screens/about/about.dart';
import './screens/events/events.dart';
import './screens/events/event_detail.dart';
import './screens/tabs/tabs.dart';
import './screens/tabs/chat/chat.dart';

void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'FFLOXER',
      home: StreamBuilder(
        stream: FirebaseAuth.instance.onAuthStateChanged, 
        builder: (ctx, userSnapshot){
          if(userSnapshot.hasData){
            return Home();
          }else{
            return Login();
          }
        },
      ),
      routes: {
        'home': (context) => Home(),
        'login': (context) => Login(),
        'check-phone': (context) => CheckPhone(), 
        'password-recovery': (context) => PasswordRecovery(),
        'create-profile': (context)=> CreateProfile(),
        'update-profile': (context)=> UpdateProfile(),
        'public-profile': (context)=> PublicProfile(),
        'search': (context)=> Search(),
        'events': (context)=> Events(),
        'event-detail': (context)=> EventDetail(),
        'about': (context)=> About(),
        'tabs': (context)=> Tabs(),
        'chat': (context)=> Chat(),
      },
      theme: ThemeData(
        backgroundColor: Color.fromRGBO(18, 18, 18, 1),
        primaryColor: Color.fromRGBO(237, 50, 55, 1),
        accentColor: Colors.red,
        canvasColor: Colors.transparent,
        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 27.0, fontWeight: FontWeight.bold, color: Colors.white),
          headline2: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold, color: Colors.white),
          headline3: TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold, color: Colors.white),
          headline4: TextStyle(fontSize: 18.0, color: Colors.white),
          headline5: TextStyle(fontSize: 15.0, color: Colors.white),
          headline6: TextStyle(fontSize: 12.0, color: Colors.white),
          bodyText1: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Color.fromRGBO(155, 155, 155, 1)),
          button: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold, color: Colors.white),
        ),
      ),
    );
  }
}