import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';

import '../../widgets/custom_button.dart';
import './check_phone.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String verificationCode;
  String phoneNumber;
  String password;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(height: size.height*0.08,),
                Image(
                  height: size.height*0.13,
                  fit: BoxFit.cover,
                  image: AssetImage("assets/images/logo-1.png"),
                ),
                SizedBox(height: size.height*0.03,),
                _loginForm(context),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5, ),
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    onPressed: (){
                      Navigator.pushNamed(context, "password-recovery");
                    },
                    child: Text("Forgot Password?", style: Theme.of(context).textTheme.bodyText1,)
                  ),
                ),
                if(_isLoading) CircularProgressIndicator(),
                if(!_isLoading)
                  CustomButton(
                    text: "Login / Register",
                    function: verifyPhone,
                  ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> verifyPhone() async{
    if(phoneNumber != null && password != null){
      setState(() {
        _isLoading = true;
      });
      final PhoneCodeAutoRetrievalTimeout autoRetrieve = (String verId){
        this.verificationCode = verId;
      };
      final PhoneCodeSent smsCodeSent = (String verId, [int forceCodeResend]){
        setState(() {
        _isLoading = false;
      });
        this.verificationCode = verId;
        Navigator.push(
          context, MaterialPageRoute(
            builder: (context) => CheckPhone(
              verificationCode: this.verificationCode,
              phoneNumber:      this.phoneNumber,
              password:         this.password,
            )
          )
        );
      };
      final PhoneVerificationCompleted verifiedSuccess = (AuthCredential auth){
        print('verified');
      };
      final PhoneVerificationFailed verifyFailed = (AuthException e) {
        print('${e.message}');
      };
      await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verifiedSuccess,
        verificationFailed: verifyFailed,
        codeSent: smsCodeSent,
        codeAutoRetrievalTimeout: autoRetrieve,
      );
    }else{
      Scaffold.of(context).showSnackBar(
        SnackBar(content: Text("Please enter your phone number and password"),
        backgroundColor: Theme.of(context).errorColor,
      ));
      return;
    }
  }

  Widget _loginForm(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: size.width*0.05),
      child: Column(
        children: [
          TextField(
            keyboardType: TextInputType.phone,
            onChanged: (value){
              this.phoneNumber = value;
            },
            decoration: InputDecoration(
              labelStyle: Theme.of(context).textTheme.bodyText1,
              labelText: "Phone:",
              enabledBorder: UnderlineInputBorder(      
                borderSide: BorderSide(color: Theme.of(context).primaryColor),   
              ),  
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
            ),
            style: Theme.of(context).textTheme.headline4,
          ),
          SizedBox(height: 8,),
          TextField(
            onChanged: (value){
              this.password = value;
            },
            decoration: InputDecoration(
              labelStyle: Theme.of(context).textTheme.bodyText1,
              labelText: "Password:",
              enabledBorder: UnderlineInputBorder(      
                borderSide: BorderSide(color: Theme.of(context).primaryColor),   
              ),  
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
            ),
            obscureText: true,
            style: Theme.of(context).textTheme.headline4,
          ),
        ],
      ),
    );
  }
}