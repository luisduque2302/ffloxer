import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:pin_code_fields/pin_code_fields.dart';

class CheckPhone extends StatefulWidget {

  CheckPhone({
    this.verificationCode,
    this.phoneNumber,
    this.password
  });

  final String verificationCode;
  final String phoneNumber;
  final String password;

  @override
  _CheckPhoneState createState() => _CheckPhoneState();

}

class _CheckPhoneState extends State<CheckPhone> {

  final _auth = FirebaseAuth.instance;
  bool _isLoading = false;
  String smsCode;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(height: size.height*0.02,),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Image(
                    alignment: Alignment.centerLeft,
                    height: size.height*0.07,
                    fit: BoxFit.contain,
                    image: AssetImage("assets/images/logo-1.png"),
                  ),
                ),
                SizedBox(height: size.height*0.1,),
                Container(
                  child: Text("Check ${widget.phoneNumber}", style: Theme.of(context).textTheme.headline4,),
                ),
                SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    "Waiting to acutomatically detect the SMS sent to ${widget.phoneNumber}", 
                    style: Theme.of(context).textTheme.headline4,
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 5,),
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    child: Text("Is the number not correct?", style: TextStyle(fontSize: 15, color: Theme.of(context).primaryColor),),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.07),
                  child: PinCodeTextField(
                    textInputType: TextInputType.number,
                    length: 6,
                    obsecureText: false,
                    animationType: AnimationType.fade,
                    textStyle: TextStyle(color: Colors.white),
                    pinTheme: PinTheme(
                      inactiveColor: Colors.red,
                      selectedColor: Colors.red,
                      activeColor: Colors.red,
                      shape: PinCodeFieldShape.underline,
                      borderRadius: BorderRadius.circular(5),
                      fieldHeight: 50,
                      fieldWidth: 40,
                      //activeFillColor: hasError ? Colors.orange : Colors.white,
                    ),
                    animationDuration: Duration(milliseconds: 300),
                    backgroundColor: Theme.of(context).backgroundColor,
                    onCompleted: (value) async {
                      signIn(context, value);
                    },
                    onChanged: (value) {
                      print(value);
                      setState(() {
                        //currentText = value;
                      });
                    },
                  ),
                ),
                SizedBox(height: 5,),
                Container(
                  child: Text("Enter the 6 digits code", style: TextStyle(fontSize: 15, color: Colors.white),),
                ),
                SizedBox(height: 8,),
                Container(
                  child: Text("Resend code", style: TextStyle(fontSize: 15, color: Colors.green),),
                ),
              ],
            ),
          ],
        )
      ),
    );
  }

  Future<void> signIn(BuildContext context, String smsCode) async {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: widget.verificationCode,
      smsCode: smsCode,
    );
    try{
      await FirebaseAuth.instance.signInWithCredential(credential).then((result) async {
        final userData = await Firestore.instance.collection("users").document(result.user.uid).get();
        //Si el usuario existe ira a la pantalla de inicio, si no a crear cuenta.
        if(userData.exists){
          Navigator.pushReplacementNamed(context, 'home');
        } else {
          Navigator.pushReplacementNamed(context, 'create-profile');
        }
      }).catchError((e){
        print(e);
      });
    } on PlatformException catch(error) {
      var message = "An error ocurred, please check your credentials!";
      if(error.message != null){
        message = error.message;
      }
      Scaffold.of(context).showSnackBar(
        SnackBar( 
          content: Text(message), 
          backgroundColor: Theme.of(context).errorColor,
        ),
      );
      setState(() {
        _isLoading = false;
      });
    } catch(error) {
      print(error);
      setState(() {
        _isLoading = false;
      });
    }
  }
}