import 'package:flutter/material.dart';

import '../../widgets/custom_button.dart';

class PasswordRecovery extends StatefulWidget {
  @override
  _PasswordRecoveryState createState() => _PasswordRecoveryState();
}

class _PasswordRecoveryState extends State<PasswordRecovery> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(height: size.height*0.02,),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Image(
                    alignment: Alignment.centerLeft,
                    height: size.height*0.07,
                    fit: BoxFit.contain,
                    image: AssetImage("assets/images/logo-1.png"),
                  ),
                ),
                SizedBox(height: size.height*0.1,),
                _recoverForm(context),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5),
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    onPressed: (){
                      Navigator.pushNamed(context, "login");
                    },
                    child: Text("Back to login", style: Theme.of(context).textTheme.bodyText1,)
                  ),
                ),
                CustomButton(
                  text: "Restore",
                  function: (){},
                ),
              ],
            ),
          ],
        )
      ),
    );
  }

  Widget _recoverForm(BuildContext context){
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: size.width*0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("Forgot Password", style: Theme.of(context).textTheme.headline2,),
          TextField(
            decoration: InputDecoration(
              labelStyle: Theme.of(context).textTheme.bodyText1,
              labelText: "Password:",
              enabledBorder: UnderlineInputBorder(      
                borderSide: BorderSide(color: Theme.of(context).primaryColor),   
              ),  
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
            ),
            obscureText: true,
            style: Theme.of(context).textTheme.headline4,
          ),
          SizedBox(height: size.height*0.05,),
          Text("OR", style: Theme.of(context).textTheme.headline2,),
          SizedBox(height: size.height*0.01,),
          TextField(
            decoration: InputDecoration(
              labelStyle: Theme.of(context).textTheme.bodyText1,
              labelText: "Password:",
              enabledBorder: UnderlineInputBorder(      
                borderSide: BorderSide(color: Theme.of(context).primaryColor),   
              ),  
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide(color: Theme.of(context).primaryColor),
              ),
            ),
            obscureText: true,
            style: Theme.of(context).textTheme.headline4,
          ),
        ],
      ),
    );
  }
}