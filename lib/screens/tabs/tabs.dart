import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../widgets/custom_bottom_menu.dart';
import '../../widgets/custom_menu_drawer.dart';
import 'groups/tab_groups.dart';
import 'messages/tab_messages.dart';
import 'taps/tab_taps.dart';

class Tabs extends StatefulWidget {
  @override
  _TabsState createState() => _TabsState();
}

class _TabsState extends State<Tabs> with TickerProviderStateMixin{

  TabController _tabController;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int _selectedMenu;
  var _isInit = false;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 3);
    _selectedMenu = 3;
  }

  @override
  void didChangeDependencies() {
    if(!_isInit){
      _tabController.index = ModalRoute.of(context).settings.arguments as int;
      if(_tabController.index == 0) _selectedMenu = 5;
      if(_tabController.index == 1) _selectedMenu = 3;
      if(_tabController.index == 2) _selectedMenu = 4;
    }
    _isInit = true;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 10,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: false,
        titleSpacing: 0.0,
        title: TabBar(
          onTap: (index){
            setState(() {
              if(index==0) _selectedMenu = 5;
              if(index==1) _selectedMenu = 3;
              if(index==2) _selectedMenu = 4;
            });
          },
          indicatorColor: Colors.white,
          controller: _tabController,
          indicatorWeight: 4,
          tabs: [
            Tab(
              icon: FaIcon(FontAwesomeIcons.users),
            ),
            Tab(
              child: Text("Messages".toUpperCase(), style: Theme.of(context).textTheme.headline5,),
            ),
            Tab(
              child: Text("Taps".toUpperCase(), style: Theme.of(context).textTheme.headline5,),
            )
          ],
        ),
        actions: [
          IconButton(
            onPressed: (){
              if(_scaffoldKey.currentState.isDrawerOpen){
                _scaffoldKey.currentState.openEndDrawer();
              }else{
                _scaffoldKey.currentState.openDrawer();
              }
            },
            icon: FaIcon(FontAwesomeIcons.bars, color: Colors.white),
          ),
        ],
        
      ),
      body: Scaffold(
        key: _scaffoldKey,
        drawer: CustomMenuDrawer(),
        bottomNavigationBar: CustomBottomMenu(current: _selectedMenu,),
        body: TabBarView(
          controller: _tabController,
          children: [
            TabGroups(),
            TabMessages(),
            TabTaps(),
          ],
        ),
      )
    );
  }
}