import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Chat extends StatefulWidget {
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  @override
  Widget build(BuildContext context) {
    final user = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(user.toString()),
        actions: [
          IconButton(
            onPressed: (){},
            icon: Icon(Icons.more_vert, size: 30,),
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        child: Stack(
          children: [
            _buildMessages(user), 
            Positioned(
              bottom: 0,
              child: _buildInputArea(context)
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMessages(user){
    return FutureBuilder(
      future: FirebaseAuth.instance.currentUser(),
      builder: (ctx, futureSnapshot){
        if(futureSnapshot.connectionState == ConnectionState.waiting){
          return Center(
            child: CircularProgressIndicator(),
          );
        }else{
          return StreamBuilder(
            stream: Firestore.instance.collection("chat/messages").where("users", arrayContains: user.toString()) .orderBy("created_at", descending: true).snapshots(),
            builder: (ctx, chatSnapshot){
              print("chatSnapshot");
              print(chatSnapshot);
              if(chatSnapshot.connectionState == ConnectionState.waiting){
                return Center(
                  child: CircularProgressIndicator(),
                );
              }else{
                final chatDocs = chatSnapshot.data.documents;
                return ListView.builder(
                  reverse: true,
                  itemCount: chatDocs.length,
                  itemBuilder: (ctx, index){
                    return Text("Mensaje");
                  }
                );
              }
            }
          );
        }
      },
    );
  }

  Widget _buildInputArea(BuildContext context){
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
      color: Theme.of(context).primaryColor,
      height: 70,
      width: size.width,
      child: Row(
        children: [
          Expanded( 
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              height: 50,
              child: TextField(
                decoration: InputDecoration(
                  suffixIcon: IconButton(
                    onPressed: (){
                      print("subir archivo");
                    },
                    icon: Icon(Icons.attach_file, color: Colors.black,),
                  ),
                  border: new OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(20.0),
                    ),
                  ),
                  filled: true,
                  hintStyle: new TextStyle(color: Colors.grey[800]),
                  hintText: "Type here...",
                  fillColor: Colors.white
                ),
              ),
            ),
          ),
          CircleAvatar(
            backgroundColor: Colors.black,
            radius: 22,
            child: IconButton(
              onPressed: (){},
              icon: Icon(Icons.send),
            ),
          ),
          
        ],
      ),
    );
  }
}