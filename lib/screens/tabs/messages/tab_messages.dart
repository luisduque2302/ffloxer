import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class TabMessages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: FirebaseAuth.instance.currentUser(),
        builder: (ctx, futureSnapshot){
          if(futureSnapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator(),
            );
          }else{
            return StreamBuilder(
              stream: Firestore.instance.collection("chat").snapshots(),
              builder: (ctx, userSnapshot){
                if(userSnapshot.connectionState == ConnectionState.waiting){
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }else{
                  final chats = userSnapshot.data.documents;
                  
                  return ListView.builder(
                    itemCount: chats.length,
                    itemBuilder: (ctx, index) => Text("test", style: TextStyle(color: Colors.white),),
                    //itemBuilder: (ctx, index) => _listItem(context, users[index]),
                  );
                  
                }
              },
            );
          }

        },
      ),
    );
  }

  Widget _listItem(BuildContext context, user){
    return Dismissible(
      key: Key(user["id"]),
      direction: DismissDirection.endToStart,
      background: Container(
        color: Theme.of(context).errorColor,
        child: Text("Delete".toUpperCase(), style: Theme.of(context).textTheme.headline3,),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
      ),
      confirmDismiss: (direction){
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text("Are you sure?"),
            content: Text("Do you want to remove the item?"),
            actions: [
              FlatButton(
                child: Text("No", style: Theme.of(context).textTheme.bodyText2,),
                onPressed: (){
                  Navigator.of(ctx).pop(false);
                },
              ),
              FlatButton(
                child: Text("Yes", style: Theme.of(context).textTheme.bodyText2,),
                onPressed: (){
                  Navigator.of(ctx).pop(true);
                },
              )
            ],
          )
        );
      },
      onDismissed: (direction){
        
      },
      child: GestureDetector(
        onTap: (){
          Navigator.of(context).pushNamed('chat', arguments: user["id"]);
        },
        child: Column(
          children: [
            ListTile(
              leading: Container(
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                    width: 1,
                  ),
                ),
                child: Image(
                  image: NetworkImage(user["image_url"]),
                  fit: BoxFit.cover,
                ),
              ),
              title: Text(user["userName"], style: Theme.of(context).textTheme.headline4,),
              subtitle: Text("Mensaje de prueba", style: Theme.of(context).textTheme.headline5,),
              trailing: Text("22:11", style: Theme.of(context).textTheme.headline6,),
            ),
            Divider(
              color: Theme.of(context).primaryColor,
              height: 1,
            ),
          ],
        ),
      ),
    );
  }
}