import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class TabTaps extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: FirebaseAuth.instance.currentUser(),
        builder: (ctx, futureSnapshot){
          if(futureSnapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator(),
            );
          }else{
            return FutureBuilder(
              future: Firestore.instance.collection("users").document(futureSnapshot.data.uid).get(),
              builder: (ctx, userSnapshot){
                if(userSnapshot.connectionState == ConnectionState.waiting){
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }else{
                  final List likes = userSnapshot.data["likes"];
                  if(likes.length > 0){
                    return StreamBuilder(
                      stream: Firestore.instance.collection("users").where("id", whereIn: userSnapshot.data["likes"]).snapshots(),
                      builder: (ctx, usersSnapshot){
                        if(usersSnapshot.connectionState == ConnectionState.waiting){
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        }else{
                          final users = usersSnapshot.data.documents;
                          return ListView.builder(
                            itemCount: users.length,
                            itemBuilder: (ctx, index) => _listItem(context, users[index]),
                          );
                        }
                      },
                    );
                  }else{
                    return Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("No one yet!", style: Theme.of(context).textTheme.headline3,),
                          SizedBox(height: 10,),
                          Text("Search for Ffloxers that you like", style: Theme.of(context).textTheme.headline4,)
                        ],
                      ),
                    );
                  }
                  
                }
              },
            );
          }

        },
      ),
    );
  }

  Widget _listItem(BuildContext context, user){
    return Dismissible(
      key: Key(user["id"]),
      direction: DismissDirection.endToStart,
      background: Container(
        color: Theme.of(context).errorColor,
        child: Text("Delete".toUpperCase(), style: Theme.of(context).textTheme.headline3,),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
      ),
      confirmDismiss: (direction){
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text("Are you sure?"),
            content: Text("Do you want to remove the item?"),
            actions: [
              FlatButton(
                child: Text("No", style: Theme.of(context).textTheme.bodyText2,),
                onPressed: (){
                  Navigator.of(ctx).pop(false);
                },
              ),
              FlatButton(
                child: Text("Yes", style: Theme.of(context).textTheme.bodyText2,),
                onPressed: (){
                  Navigator.of(ctx).pop(true);
                },
              )
            ],
          )
        );
      },
      onDismissed: (direction){
        
      },
      child: Column(
        children: [
          ListTile(
            leading: Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
              ),
              child: Image(
                image: NetworkImage(user["image_url"]),
                fit: BoxFit.cover,
              ),
            ),
            title: Text(user["userName"], style: Theme.of(context).textTheme.headline4,),
            subtitle: Text("Mensaje de prueba", style: Theme.of(context).textTheme.headline5,),
            trailing: Icon(Icons.favorite, color: Theme.of(context).primaryColor, size: 30,),
          ),
          Divider(
            color: Theme.of(context).primaryColor,
            height: 1,
          ),
        ],
      ),
    );
  }
}