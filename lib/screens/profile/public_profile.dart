import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../widgets/custom_bottom_menu.dart';

class PublicProfile extends StatefulWidget {

  @override
  _PublicProfileState createState() => _PublicProfileState();
}

class _PublicProfileState extends State<PublicProfile> {

  PageController pageController = PageController(viewportFraction: 1.0);
  bool showNotesIcon = false;
  int _imageIndex = 0;
  bool _isBlocked = false;
  bool _isLiked = false;
  bool _init = false;
  var _sessionUser;
  var _sessionUserData;

  @override
  void didChangeDependencies() async {
    /*
    _currentUser = await FirebaseAuth.instance.currentUser();
    _currentUserData = await Firestore.instance.collection("users").document(_currentUser.uid).get();
    final user = ModalRoute.of(context).settings.arguments;
    
    final userId = user["id"];
    final List likes = _currentUserData.data["likes"];

    print(likes.contains (user));
    */
    //print(user);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    
    final user = ModalRoute.of(context).settings.arguments;
    _loadValues(context, user);
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            _content(user),
            _customAppBar(user)
          ],
        ),
      )
    );
  }

  Widget _content(user){
    final size = MediaQuery.of(context).size;
    final items = [
      if(user['image_url'] != null) user['image_url'].toString(),
      if(user['image_url1'] != null) user['image_url1'].toString(),
      if(user['image_url2'] != null) user['image_url2'].toString(),
      if(user['image_url3'] != null) user['image_url3'].toString(),
      if(user['image_url4'] != null) user['image_url4'].toString()
    ];
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: ListView(
        children: [
          Container(
            height: size.height-35,
            child: Stack(
              children: [
                _imagesSlider(user, items),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [ 
                    _userOptions(user, items),
                  ],
                ),
              ],
            ),
          ),
          _userInfo(user)
        ],
      ),
    );
  }

  Widget _customAppBar(user){
    final size = MediaQuery.of(context).size;
   
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 7, vertical: 3),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: (){
                  Navigator.of(context).pop();
                },
                icon: Icon(Icons.arrow_back_ios, size: 19, color: Colors.white),
              ),
              Image(
                alignment: Alignment.centerLeft,
                height: size.height*0.07,
                fit: BoxFit.contain,
                image: AssetImage("assets/images/logo-1.png"),
              ),
              Row(
                children: [
                  if(showNotesIcon) GestureDetector(
                      onTap: (){},
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Icon(FontAwesomeIcons.fileAlt, size: 20, color: Colors.white),
                      ),
                    ),
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        showNotesIcon = !showNotesIcon;
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Icon(Icons.star, size: 20, color: Colors.white),
                    ),
                  ),
                  SizedBox(width: 2,),
                  GestureDetector(
                    onTap: (){},
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Icon(Icons.not_interested, size: 19, color: Colors.white),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: (){
                  _addBlocked(user['id']);
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: (_isBlocked) ? Colors.red : Colors.grey,
                  ),
                  padding: const EdgeInsets.all(4.0),
                  margin: EdgeInsets.symmetric(horizontal:5),
                  child: Icon(Icons.lock, size: 17, color: Colors.white),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _imagesSlider(user, items){
    final size = MediaQuery.of(context).size;
    return Container(
      height: double.infinity,
      child: PageView.builder(
        onPageChanged: (val){
          setState(() {
            _imageIndex = val;
          });
        },
        controller: pageController,
        itemCount: items.length,
        itemBuilder: (context, index){
          return Container(
            height: double.infinity,
            width: double.infinity,
            child: Image(
              fit: BoxFit.cover,
              image: NetworkImage(items[index]),
            ),
          );
        }
      ),
    );
  }

  Widget _userOptions(user, List items){
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.only(left: 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical:10),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: items.map((e){
                var index = items.indexOf(e);
                return Container(
                  margin: EdgeInsets.symmetric(horizontal: size.width*0.018),
                  height: 6,
                  color: (_imageIndex == index) ? Colors.white : Colors.grey,
                  width: size.width*0.15,
                );
              }).toList(),
            ),
          ),
          Text("${user['userName'].toString()} - ${user['age'].toString()}", style: Theme.of(context).textTheme.headline2,),
          Container(
            color: Color.fromRGBO(50, 50, 50, 0.4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          radius: 4,
                          backgroundColor: Colors.green,
                        ),
                        SizedBox(width: 5,),
                        Text("Online - 17 min ago", style: Theme.of(context).textTheme.headline6,)
                      ],
                    ),
                    Text("Distance 12 km", style: Theme.of(context).textTheme.headline6,),
                  ],
                ),
                Container(
                  height: 36,
                  margin: EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: (){
                          _addLike(user['id']);
                        },
                        icon: FaIcon(FontAwesomeIcons.fistRaised, size: 28, color: _isLiked ? Theme.of(context).primaryColor : Colors.white,),
                      ),
                      IconButton(
                        onPressed: (){},
                        icon: FaIcon(FontAwesomeIcons.commentDots, size: 28, color: Colors.white,),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _userInfo(user){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      child: Column(
        children: [
          _userInfoItem("UserName", "${user['userName'].toString()}"),
          Divider(color: Colors.white,),
          _userInfoItem("Age", "${user['age'].toString()}"),
          Divider(color: Colors.white,),
          _userInfoItem("Hometown", "${user['hometown'].toString()}"),
          Divider(color: Colors.white,),
          _userInfoItem("Height", "${user['height']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Weight", "${user['weight']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Body", "${user['body']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Body Hair", "${user['bodyHair']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Sex", "${user['sex']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Ethnicity", "${user['race']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Partner Status", "${user['partnerStatus']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Position", "${user['position']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Safer Sex", "${user['saferSex']}"),
          Divider(color: Colors.white,),
          _userInfoItem("I am", "${user['iAm']}"),
          Divider(color: Colors.white,),
          _userInfoItem("I like", "${user['iLike']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Fetish", "${user['fetish']}"),
          Divider(color: Colors.white,),
          _userInfoItem("Dirty", "${user['dirty']}"),
          Divider(color: Colors.white,),
          _userInfoItem("S & M", "${user['sYm']}"),
          Divider(color: Colors.white,),
          Row(
            children: [
              Text("About this FFloxer", style: Theme.of(context).textTheme.bodyText1, textAlign: TextAlign.left)
            ],
          ),
          Text(
            "${user['about'].toString()}",
            style: Theme.of(context).textTheme.headline4,
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    );
  }

  Widget _userInfoItem(String title, String value){
    return Row(
      children: <Widget>[
        Expanded(
          flex: 4, // 20%
          child: Text("$title:", style: Theme.of(context).textTheme.bodyText1,),
        ),
        Expanded(
          flex: 6, // 60%
          child: Text(value, style: Theme.of(context).textTheme.headline4),
        ),
      ],
    );
  }

  void _loadValues(BuildContext context, user) async{
    if(!_init){
      _sessionUser = await FirebaseAuth.instance.currentUser();
      _sessionUserData = await Firestore.instance.collection("users").document(_sessionUser.uid).get();
      
      final userId = user["id"];
      final List likes = _sessionUserData.data["likes"];
      final List blocked = _sessionUserData.data["blocked"];
      setState(() {
        _isLiked = likes.contains(userId);
        _isBlocked = blocked.contains(userId);
      });
    }
    _init = true;
  }

  void _addLike(String id) async {
    setState(() {
      _isLiked = !_isLiked;
    });
    final List likes = _sessionUserData.data["likes"]; 
    if(likes.contains(id)){
      likes.removeWhere((item) => item == id);
      await Firestore.instance.collection("users").document(_sessionUser.uid).updateData({"likes": likes});
    }else{
      likes.insert(0, id);
      await Firestore.instance.collection("users").document(_sessionUser.uid).updateData({"likes": likes});
    }
  }

  void _addBlocked(String id) async {
    setState(() {
      _isBlocked = !_isBlocked;
    });
    final List blocked = _sessionUserData.data["blocked"]; 
    if(blocked.contains(id)){
      blocked.removeWhere((item) => item == id);
      await Firestore.instance.collection("users").document(_sessionUser.uid).updateData({"blocked": blocked});
    }else{
      blocked.insert(0, id);
      await Firestore.instance.collection("users").document(_sessionUser.uid).updateData({"blocked": blocked});
    }
  }
}