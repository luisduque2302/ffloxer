import 'package:flutter/material.dart';
import 'dart:io';

import 'package:image_picker/image_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

import '../../widgets/custom_button.dart';
import '../../widgets/custom_text_field.dart';
import '../../widgets/custom_dropdown.dart';

class CreateProfile extends StatefulWidget {
  @override
  _CreateProfileState createState() => _CreateProfileState();
}

class _CreateProfileState extends State<CreateProfile> {

  File _image;
  File _image1;
  File _image2;
  File _image3;
  File _image4;
  String _userName;
  String _age;
  String _hometown;
  String _sex;
  String _height;
  String _weight;
  String _body;
  String _bodyHair;
  String _race;
  String _email;
  String _emailConfirm;
  String _phone;
  String _instagram;
  String _twitter;
  String _position;
  String _fisting;
  String _saferSex;
  String _partnerStatus;
  String _about;
  String _dirty;
  String _sym;

  final picker = ImagePicker();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Profile"),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ListView(
                    children: [
                      SizedBox(height: 10,),
                      _imagesSection(context),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                          color: Color.fromRGBO(85, 5, 5, 1),
                          onPressed: (){},
                          child: Text("Click to Access / Uploades to Provate Album", style: Theme.of(context).textTheme.headline5,),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(flex: 2, child: CustomTextField(label: "Username", function: (val){ _userName = val; },)),
                          SizedBox(width: 8,),
                          Expanded(flex: 1, child: CustomTextField(label: "Age", function: (val){ _age = val; })),
                          SizedBox(width: 8,),
                          Expanded(flex: 2, child: CustomTextField(label: "Hometown", function: (val){ _hometown = val; })),
                        ],
                      ),
                      Expanded(
                        flex: 2,
                        child: DropdownWidget(
                          title: "Sex",
                          items: [
                            "Male", 
                            "Female",
                            "Trans",
                            "Non-Gender Conforming",
                          ],
                          currentItem: _sex,
                          itemCallBack: (String val) {
                            _sex = val;
                          },
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "Height",
                              items: [
                                "Less than 141 cm", "141 cm", "142 cm", "143 cm", "144 cm", "145 cm", "146 cm", "147 cm", "148 cm", "149 cm", "150 cm",
                                "151 cm", "152 cm", "153 cm", "154 cm", "155 cm", "156 cm", "157 cm", "158 cm", "159 cm", "160 cm",
                                "161 cm", "162 cm", "163 cm", "164 cm", "165 cm", "166 cm", "167 cm", "168 cm", "169 cm", "170 cm",
                                "171 cm", "172 cm", "173 cm", "174 cm", "175 cm", "176 cm", "177 cm", "178 cm", "179 cm", "180 cm",
                                "181 cm", "182 cm", "183 cm", "184 cm", "185 cm", "186 cm", "187 cm", "188 cm", "189 cm", "190 cm",
                                "191 cm", "192 cm", "193 cm", "194 cm", "195 cm", "196 cm", "197 cm", "198 cm", "199 cm", "200 cm",
                                "201 cm", "202 cm", "203 cm", "204 cm", "205 cm", "206 cm", "207 cm", "208 cm", "209 cm", "210 cm", "More than 210 cm",
                              ],
                              currentItem: _height,
                              itemCallBack: (String val) { 
                                _height = val;
                              },
                            ),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "Weight",
                              items: [
                                "Less than 41 kg", "41 kg", "42 kg", "43 kg", "44 kg", "45 kg", "46 kg", "47 kg", "48 kg", "49 kg", "50 kg",
                                "51 kg", "52 kg", "53 kg", "54 kg", "55 kg", "56 kg", "57 kg", "58 kg", "59 kg", "60 kg",
                                "61 kg", "62 kg", "63 kg", "64 kg", "65 kg", "66 kg", "67 kg", "68 kg", "69 kg", "70 kg",
                                "71 kg", "72 kg", "73 kg", "74 kg", "75 kg", "76 kg", "77 kg", "78 kg", "79 kg", "80 kg",
                                "81 kg", "82 kg", "83 kg", "84 kg", "85 kg", "86 kg", "87 kg", "88 kg", "89 kg", "90 kg",
                                "91 kg", "92 kg", "93 kg", "94 kg", "95 kg", "96 kg", "97 kg", "98 kg", "99 kg", "100 kg",
                                "101 kg", "102 kg", "103 kg", "104 kg", "105 kg", "106 kg", "107 kg", "108 kg", "109 kg", "110 kg", "More than 110 kg",
                              ],
                              currentItem: _weight,
                              itemCallBack: (String val) {
                                _weight = val;
                              },
                            ),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "Body",
                              items: [
                                "Slim",
                                "Average",
                                "Atletic",
                                "Muscular",
                                "Belly",
                                "Stocky"
                              ],
                              currentItem: _body,
                              itemCallBack: (String val) {
                                _body = val;
                              },
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "Body Hair",
                              items: [
                                "Smooth",
                                "Shaved",
                                "Not very hairy",
                                "Hairy",
                                "Very hairy",
                              ],
                              currentItem: _bodyHair,
                              itemCallBack: (String val) {
                                _bodyHair = val;
                              },
                            ),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            flex: 2,
                            child: CustomTextField(label: "Race / Ethnicity", function: (val){ _race = val; },),
                          ),
                        ],
                      ),
                      CustomTextField(label: "Email", function: (val){ _email = val; },),
                      CustomTextField(label: "Email Confirmation", function: (val){ _emailConfirm = val; }),
                      CustomTextField(label: "Mobile number", function: (val){ _phone = val; }),
                      CustomTextField(label: "Instagram", function: (val){ _instagram = val; }),
                      CustomTextField(label: "Twiter", function: (val){ _twitter = val; }),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "Position",
                              items: [
                                "Top only",
                                "More top",
                                "Versatile",
                                "More passive",
                                "Only passive",
                                "No anal",
                              ],
                              currentItem: _position,
                              itemCallBack: (String val) {
                                _position = val;
                              },
                            ),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "Fisting",
                              items: [
                                "10 % pasive - 90 % active",
                                "20 % pasive - 80 % active",
                                "30 % pasive - 70 % active",
                                "40 % pasive - 60 % active",
                                "50 % pasive - 50 % active",
                                "60 % pasive - 40 % active",
                                "70 % pasive - 30 % active",
                                "80 % pasive - 20 % active",
                                "90 % pasive - 10 % active",
                              ],
                              currentItem: _fisting,
                              itemCallBack: (String val) {
                                _fisting = val;
                              },
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "Safer Sex",
                              items: [
                                "Safe",
                                "Let's talk",
                                "Condom",
                                "PrEP",
                                "PrEP and condom",
                                "TasP"
                              ],
                              currentItem: _saferSex,
                              itemCallBack: (String val) {
                                _saferSex = val;
                              },
                            ),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "Partner Status",
                              items: [
                                "Single",
                                "Partnered",
                                "Open relationship",
                                "Married"
                              ],
                              currentItem: _partnerStatus,
                              itemCallBack: (String val) {
                                _partnerStatus = val;
                              },
                            ),
                          ),
                        ],
                      ),
                      _iAmSection(),
                      Divider(color: Colors.grey,),
                      _iLikeSection(),
                      Divider(color: Colors.grey,),
                      _fetishSection(),
                      Divider(color: Colors.grey,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "Dirty",
                              items: [
                                "Dirty",
                                "No dirty",
                                "WS only",
                              ],
                              currentItem: _dirty,
                              itemCallBack: (String val) {
                                _dirty = val;
                              },
                            ),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            flex: 2,
                            child: DropdownWidget(
                              title: "S & M",
                              items: [
                                "SM",
                                "Soft SM",
                                "Kein SM",
                              ],
                              currentItem: _sym,
                              itemCallBack: (String val) {
                                _sym = val;
                              },
                            )
                          ),
                        ],
                      ),
                      CustomTextField(label: "About Me", function: (val){ _about = val; }, maxLines: 5,),
                      SizedBox(height: 30,),
                    ],
                  ),
                ),
              ),
              if(_isLoading) CircularProgressIndicator(),
              if(!_isLoading)
                CustomButton(
                  text: "Create",
                  function: _createUser,
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _iAmSection(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("I am:", style: Theme.of(context).textTheme.bodyText1,),
        Wrap(
          children: [
            _propertyItem("Bear"),
            _propertyItem("Guy Next Door"),
            _propertyItem("Daddy"),
            _propertyItem("Geek"),
            _propertyItem("Chubb"),
            _propertyItem("Chaser"),
            _propertyItem("Hunter"),
            _propertyItem("Leather"),
            _propertyItem("Jock"),
            _propertyItem("Discreet"),
            _propertyItem("Nerd"),
            _propertyItem("Soldier"),
            _propertyItem("Poz"),
            _propertyItem("Dominant"),
            _propertyItem("Military"),
            _propertyItem("Submissive"),
            _propertyItem("Poz"),
            _propertyItem("Otter"),
            _propertyItem("Bisexual"),
            _propertyItem("Transgender"),
            _propertyItem("Queer"),
            _propertyItem("Drag"),
          ],
        ),
      ],
    );
  }

  Widget _iLikeSection(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("I like:", style: Theme.of(context).textTheme.bodyText1,),
        Wrap(
          children: [
            _propertyItem("Bear"),
            _propertyItem("Guy Next Door"),
            _propertyItem("Daddy"),
            _propertyItem("Geek"),
            _propertyItem("Chubb"),
            _propertyItem("Chaser"),
            _propertyItem("Hunter"),
            _propertyItem("Leather"),
            _propertyItem("Jock"),
            _propertyItem("Discreet"),
            _propertyItem("Nerd"),
            _propertyItem("Soldier"),
            _propertyItem("Poz"),
            _propertyItem("Dominant"),
            _propertyItem("Military"),
            _propertyItem("Submissive"),
            _propertyItem("Poz"),
            _propertyItem("Otter"),
            _propertyItem("Bisexual"),
            _propertyItem("Transgender"),
            _propertyItem("Queer"),
            _propertyItem("Drag"),
          ],
        ),
      ],
    );
  }

  Widget _fetishSection(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Fetish:", style: Theme.of(context).textTheme.bodyText1,),
        Wrap(
          children: [
            _propertyItem("Leather"),
            _propertyItem("Rubber"),
            _propertyItem("Sports Gear"),
            _propertyItem("Underwear"),
            _propertyItem("Tatoos"),
            _propertyItem("Piercing"),
            _propertyItem("Military"),
            _propertyItem("Bondage"),
            _propertyItem("Sneakers"),
            _propertyItem("Socks"),
            _propertyItem("Formal Dress"),
            _propertyItem("Worker"),
            _propertyItem("Boss"),
            _propertyItem("Skins"),
            _propertyItem("Punks"),
            _propertyItem("Jeans"),
            _propertyItem("Doctor games"),
            _propertyItem("Sex toys"),
            _propertyItem("Feet"),
            _propertyItem("Masks"),
          ],
        ),
      ],
    );
  }

  Widget _propertyItem(String label){
    return GestureDetector(
      onTap: (){},
      child: Container(
        padding: EdgeInsets.all(7),
        margin: EdgeInsets.all(6),
        color: Colors.red,
        child: Text(label, style: Theme.of(context).textTheme.headline5,),
      ),
    );
  }

  Widget _imagesSection(BuildContext context){
    final size = MediaQuery.of(context).size;
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _uploadImageItem(size.width*0.45, size.width*0.48, 70, (){getImage(1);}, _image),
          Container(
            child: Row(
              children: [
                Column(
                  children: [
                    _uploadImageItem(size.width*0.21, size.width*0.21, 40, (){getImage(2);}, _image1),
                    _uploadImageItem(size.width*0.21, size.width*0.21, 40, (){getImage(3);}, _image2),
                  ],
                ),
                Column(
                  children: [
                    _uploadImageItem(size.width*0.21, size.width*0.21, 40, (){getImage(4);}, _image3),
                    _uploadImageItem(size.width*0.21, size.width*0.21, 40, (){getImage(5);}, _image4),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _uploadImageItem(double width, double height, double iconSize, Function funtion, File file){
    return GestureDetector(
      onTap: funtion,
      child: Container(
        margin: EdgeInsets.all(5),
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Color.fromRGBO(112, 112, 112, 1)
        ),
        child: (file != null) 
          ? Image(
            image: FileImage(file),
            fit: BoxFit.cover,
          ) 
          : Icon(
            Icons.file_upload, 
            color: Color.fromRGBO(172, 172, 172, 1), 
            size: iconSize,
          ),
      ),
    );
  }

  Future getImage(int type) async {
    final pickedFile = await picker.getImage(
      source: ImageSource.camera,
      imageQuality: 95,
      maxWidth: 700,
    );
    setState(() {
      if(type==1) _image = File(pickedFile.path);
      else if(type==2) _image1 = File(pickedFile.path);
      else if(type==3) _image2 = File(pickedFile.path);
      else if(type==4) _image3 = File(pickedFile.path);
      else if(type==5) _image4 = File(pickedFile.path);
    });
  }

  void _createUser() async{
    setState(() {
      _isLoading = true;
    });
    final user = await FirebaseAuth.instance.currentUser();
    final ref = FirebaseStorage.instance .ref().child("user_images").child(user.uid + ".jpg");
    final ref1 = FirebaseStorage.instance .ref().child("user_images").child(user.uid + "1.jpg");
    final ref2 = FirebaseStorage.instance .ref().child("user_images").child(user.uid + "2.jpg");
    final ref3 = FirebaseStorage.instance .ref().child("user_images").child(user.uid + "3.jpg");
    final ref4 = FirebaseStorage.instance .ref().child("user_images").child(user.uid + "4.jpg");

    String url;
    if(_image != null){
      await ref.putFile(_image).onComplete;
      url = await ref.getDownloadURL();
    }
    String url1;
    if(_image1 != null){
      await ref1.putFile(_image1).onComplete;
      url1 = await ref1.getDownloadURL();
    }
    String url2;
    if(_image2 != null){
      await ref2.putFile(_image2).onComplete;
      url2 = await ref2.getDownloadURL();
    }
    String url3;
    if(_image3 != null){
      await ref3.putFile(_image3).onComplete;
      url3 = await ref3.getDownloadURL();
    }
    String url4;
    if(_image4 != null){
      await ref4.putFile(_image4).onComplete;
      url4 = await ref4.getDownloadURL();
    } 
   
    await Firestore.instance
            .collection("users")
            .document(user.uid)
            .setData({
              "id": user.uid,
              "userName": _userName,
              "age": _age,
              "hometown": _hometown,
              "sex": _sex,
              "height": _height,
              "weight": _weight,
              "body": _body,
              "bodyHair": _bodyHair,
              "race": _race,
              "email": _email,
              "phone": _phone,
              "instagram": _instagram,
              "twitter": _twitter,
              "position": _position,
              "fisting": _fisting,
              "saferSex": _saferSex,
              "partnerStatus": _partnerStatus,
              "created_at": DateTime.now().toString(),
              "image_url": url,
              "image_url1": url1,
              "image_url2": url2,
              "image_url3": url3,
              "image_url4": url4,
              "iAm": [],
              "iLike": [],
              "fetish": [],
              "dirty": _dirty,
              "sYm": _sym,
              "about": _about,
              "comments": [],
              "likes": [],
              "blocked": [],
            });
    Navigator.pushReplacementNamed(context, 'home');
  }
}