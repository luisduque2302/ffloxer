import 'package:ffloxer/widgets/custom_button.dart';
import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../widgets/custom_bottom_menu.dart';
import '../../widgets/custom_popup_bottom_menu_filter.dart';
import '../../widgets/custom_menu_drawer.dart';

class EventDetail extends StatefulWidget {
  @override
  _EventDetailState createState() => _EventDetailState();
}

class _EventDetailState extends State<EventDetail> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final event = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      primary: true,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Event Detail"),
      ),
      body: Container(
        child: Stack(
          children: [
            _buildContent(context, event),
            Positioned(
              bottom: 0,
              child: _buildCustomButton()
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildContent(BuildContext context, event) {
    return ListView(
      children: [
        Container(
          child: Stack(
            children: [
              Image(
                image: NetworkImage(event["image"]),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
          color: Theme.of(context).backgroundColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(event["title"], style: Theme.of(context).textTheme.headline4,),
                  Row(
                    children: [
                      Icon(Icons.location_on, color: Colors.white,),
                      Text("Less than 100 Km", style: Theme.of(context).textTheme.bodyText1,)
                    ],
                  ),
                  Text("April 24, 2020  -  May 3, 2020", style: Theme.of(context).textTheme.bodyText1,),
                  Text("14:00 H", style: Theme.of(context).textTheme.bodyText1,),
                ],
              ),
              Column(
                children: [
                  GestureDetector(
                    onTap: (){},
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Icon(Icons.favorite_border, color: Theme.of(context).primaryColor, size: 25,)
                    ),
                  ),
                  GestureDetector(
                    onTap: (){},
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Icon(Icons.check_circle_outline, color: Colors.green, size: 25,)
                    ),
                  ),
                  GestureDetector(
                    onTap: (){},
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Icon(Icons.info_outline, color: Colors.yellow, size: 25,)
                    ),
                  ),
                ]
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Interested people", style: Theme.of(context).textTheme.headline4,),
              Container(
                padding: EdgeInsets.only(top: 5, bottom: 0),
                height: 80,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                      padding: EdgeInsets.all(3),
                      child: CircleAvatar(
                        radius: 32,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(3),
                      child: CircleAvatar(
                        radius: 32,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(3),
                      child: CircleAvatar(
                        radius: 32,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(3),
                      child: CircleAvatar(
                        radius: 32,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(3),
                      child: CircleAvatar(
                        radius: 32,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(3),
                      child: CircleAvatar(
                        radius: 32,
                      ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: (){},
                child: Text("See all interested", style: Theme.of(context).textTheme.headline4,)
              ),
              SizedBox(height: 15,),
              Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                style: Theme.of(context).textTheme.bodyText1,),
            ],
          ),
        ),
        SizedBox(height: 70,),
        
      ],
    );
  }

  Widget _buildCustomButton(){
    return CustomButton(
      text: "Get Ticket / Register",
      function: (){},
    );
  }
}