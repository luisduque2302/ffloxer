import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../widgets/custom_bottom_menu.dart';
import '../../widgets/custom_menu_drawer.dart';
import '../../widgets/custom_popup_bottom_menu_filter.dart';

class Events extends StatefulWidget {
  @override
  _EventsState createState() => _EventsState();
}

class _EventsState extends State<Events> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: CustomPopupBottomMenuFilter(),
        title: Text("FFloxer World"),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: (){
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios),
          )
        ],
      ),
      body: Scaffold(
        key: _scaffoldKey,
        drawer: CustomMenuDrawer(),
        bottomNavigationBar: CustomBottomMenu(current: 2,),
        body: FutureBuilder(
          future: FirebaseAuth.instance.currentUser(),
          builder: (ctx, futureSnapshot){
            if(futureSnapshot.connectionState == ConnectionState.waiting){
              return Center(
                child: CircularProgressIndicator(),
              );
            }else{
              return StreamBuilder(
                stream: Firestore.instance.collection("events").orderBy("startDate", descending: true).snapshots(),
                builder: (ctx, eventSnapshot){
                  if(eventSnapshot.connectionState == ConnectionState.waiting){
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }else{
                    final eventDocs = eventSnapshot.data.documents;
                    return ListView.builder(
                      itemCount: eventDocs.length,
                      itemBuilder: (ctx, index)=> _eventItem(eventDocs[index]),
                    );
                  }
                },
              );
            }
          }
        ),
      )
    );
  }

  Widget _eventItem(item){
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed("event-detail", arguments: item);
      },
      child: Container(
        child: Column(
          children: [
            Container(
              child: Stack(
                children: [
                  Image(
                    image: NetworkImage("https://r-cf.bstatic.com/images/hotel/max1024x768/154/154578319.jpg"),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text(item['title'], style: Theme.of(context).textTheme.headline4,),
                      Row(
                        children: [
                          Icon(Icons.location_on, color: Colors.white,),
                          Text("Less than 100 Km", style: Theme.of(context).textTheme.bodyText1,)
                        ],
                      ),
                    ],
                  ),
                  IconButton(
                    onPressed: (){},
                    icon: Icon(Icons.favorite_border, color: Theme.of(context).primaryColor, size: 30,),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}