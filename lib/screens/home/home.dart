import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../widgets/custom_bottom_menu.dart';
import '../../widgets/custom_popup_bottom_menu_filter.dart';
import '../../widgets/custom_menu_drawer.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: true,
      appBar: AppBar(
        titleSpacing: 0.0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              children: [
                CustomPopupBottomMenuFilter(),
                GestureDetector(
                  onTap: (){
                    Navigator.pushNamed(context, "search");
                  },
                  child: FaIcon(FontAwesomeIcons.dotCircle, color: Colors.white, size: 20)
                ),
              ],
            ),
            Expanded(
              child: Text(
                "Who's Nearby", 
                style: Theme.of(context).textTheme.headline2,
                textAlign: TextAlign.center,
              )
            ),
          ],
        ),
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: (){
              if(_scaffoldKey.currentState.isDrawerOpen){
                _scaffoldKey.currentState.openEndDrawer();
              }else{
                _scaffoldKey.currentState.openDrawer();
              }
            },
            icon: FaIcon(FontAwesomeIcons.bars, color: Colors.white),
          ),
        ],
      ),
      body: Scaffold(
        key: _scaffoldKey,
        drawer: CustomMenuDrawer(),
        bottomNavigationBar: CustomBottomMenu(current: 1,),
        body: FutureBuilder(
          future: FirebaseAuth.instance.currentUser(),
          builder: (ctx, futureSnapshot){
            if(futureSnapshot.connectionState == ConnectionState.waiting){
              return Center(
                child: CircularProgressIndicator(),
              );
            }else{
              return StreamBuilder(
                stream: Firestore.instance.collection("users").orderBy("created_at", descending: true).snapshots(),
                builder: (ctx, userSnapshot){
                  if(userSnapshot.connectionState == ConnectionState.waiting){
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }else{
                    final userDocs = userSnapshot.data.documents;
                    return GridView.builder(
                      itemCount: userDocs.length,
                      itemBuilder: (ctx, index) => profileItem(context, userDocs[index]) ,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3, 
                        //childAspectRatio: 2/3, 
                        crossAxisSpacing: 1, 
                        mainAxisSpacing: 1
                      ),
                    );
                  }
                }
              );
            }
          },
        ),
      ),
    );
  }
  
  Widget profileItem(BuildContext context, user){
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, 'public-profile', arguments: user);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            width: 0.5
          )
        ),
        child: Stack(
          children: [ 
            FadeInImage(
              width: size.width*0.33,
              height: size.width*0.4,
              fit: BoxFit.cover,
              placeholder: AssetImage(
                "assets/images/logo-1.png"
              ),
              image: NetworkImage(
                user["image_url"]
              ),
            ),
            Positioned(
              bottom: 8,
              left: 8,
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 4,
                    backgroundColor: Colors.green,
                  ),
                  SizedBox(width: 5,),
                  Text(user["userName"].toUpperCase(), style: Theme.of(context).textTheme.headline6,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}