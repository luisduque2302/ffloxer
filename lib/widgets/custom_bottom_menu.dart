import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CustomBottomMenu extends StatelessWidget {

  CustomBottomMenu({
    this.current
  });

  final int current;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      color: Theme.of(context).primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _iconButtonItem(context, (current == 1), FontAwesomeIcons.dotCircle, "home"),
          _iconButtonItem(context, (current == 2), FontAwesomeIcons.calendar, "events"),
          _iconButtonItem(context, (current == 3), FontAwesomeIcons.commentDots, "tabs", tab: 1),
          _iconButtonItem(context, (current == 4), FontAwesomeIcons.heart, "tabs", tab: 2),
          _iconButtonItem(context, (current == 5), FontAwesomeIcons.users, "tabs", tab: 0),
        ],
      ),
    );
  }

  Widget _iconButtonItem(BuildContext context, bool selected, IconData icon, String route, {int tab=1}){
    final size = MediaQuery.of(context).size;
    return IconButton(
      onPressed: (){
        Navigator.pushNamed(context, route, arguments: tab);
      },
      icon: FaIcon(icon, color: Colors.white, size: selected ? 35 : 20,),
    );
  }
}