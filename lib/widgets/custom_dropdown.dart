import 'package:flutter/material.dart';

class DropdownWidget extends StatefulWidget {
  final String title;
  final List<String> items;
  final ValueChanged<String> itemCallBack;
  final String currentItem;

  DropdownWidget({
    this.title,
    this.items,
    this.itemCallBack,
    this.currentItem,
  });

  @override
  State<StatefulWidget> createState() => _DropdownState(currentItem);
}

class _DropdownState extends State<DropdownWidget> {
  List<DropdownMenuItem<String>> dropDownItems = [];
  String currentItem;

  _DropdownState(this.currentItem);

  @override
  void initState() {
    super.initState();
    for (String item in widget.items) {
      dropDownItems.add(DropdownMenuItem(
        value: item,
        child: Text( item, style: TextStyle(fontSize: 16)),
      ));
    }
  }

  @override
  void didUpdateWidget(DropdownWidget oldWidget) {
    if (this.currentItem != widget.currentItem) {
      setState(() {
        this.currentItem = widget.currentItem;
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 5,),
          Text(widget.title, style: Theme.of(context).textTheme.bodyText1,),
          DropdownButton(
            dropdownColor: Colors.black,
            style: Theme.of(context).textTheme.headline4,
            value: currentItem,
            isExpanded: true,
            items: dropDownItems,
            onChanged: (selectedItem) => setState(() {
              currentItem = selectedItem;
              widget.itemCallBack(currentItem);
            }),
          ),
        ],
      ),
    );
  }
}