import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CustomMenuDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Firestore.instance.collection("users").reference();
    
    return Drawer(
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.5),
              spreadRadius: 4,
              blurRadius: 10,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(18),
            bottomRight: Radius.circular(18)
          ),
          child: Container(
            color: Theme.of(context).primaryColor,
            child: ListView( 
              padding: EdgeInsets.zero,
              children: <Widget>[
                _drawerHeader(context),
                _itemList(context, Icons.search, "Search FFloxers", "search"),
                _itemList(context, Icons.group, "My Groups", "tabs"),
                _itemList(context, Icons.calendar_today, "Events", "events"),
                _itemList(context, FontAwesomeIcons.heart, "Taps", "tabs"),
                _itemList(context, Icons.person, "Report User", "search"),
                _itemList(context, null, "About FFloxer", "about"),
                _itemList(context, null, "Log Out", "logout"),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _drawerHeader(BuildContext context){
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height*0.2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: size.width*0.25,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(15),
                bottomLeft: Radius.circular(15)
              ),
              child: Image(
                width: 75,
                height: 75,
                fit: BoxFit.cover,
                image: NetworkImage(
                  "https://st2.depositphotos.com/1006318/5909/v/450/depositphotos_59095205-stock-illustration-businessman-profile-icon.jpg"
                ),
              ),
            ),
          ),
          Container(
            width: size.width*0.45,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Aurélien Solomon", style: Theme.of(context).textTheme.headline1,),
                Text("VERIFIED USER", style: Theme.of(context).textTheme.headline6,)
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _itemList(BuildContext context, IconData icon, String text, String route){
    return GestureDetector(
      onTap: () async {
        if(route != "logout"){
          Navigator.pushNamed(context, route); 
        }else{
          await FirebaseAuth.instance.signOut().then((value) => Navigator.pushNamed(context, "login") );
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
        child: Row(
          children: [
            if(icon != null) Icon(icon, color: Colors.white, size: 24,),
            SizedBox(width: 10),
            Text(text, style: Theme.of(context).textTheme.headline5,),
          ],
        ),
      ),
    );
  }
}
