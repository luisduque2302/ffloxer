import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {

  CustomButton({
    this.text,
    this.function
  });

  final String text;
  final Function function;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: 55,
      child: RaisedButton(
        color: Theme.of(context).primaryColor,
        onPressed: function,
        child: Text(text.toUpperCase(), style: Theme.of(context).textTheme.button,),
      ),
    );
  }
}