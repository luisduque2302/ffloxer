import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CustomPopupBottomMenuFilter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: FaIcon(FontAwesomeIcons.filter, color: Colors.white, size: 20,),
      color: Theme.of(context).primaryColor,
      itemBuilder: (context){
        return [
          PopupMenuItem(
            value: "1",
            child: Text("Active now", style: Theme.of(context).textTheme.headline3,)
          ),
          PopupMenuItem(
            value: "2",
            child: Text("report", style: Theme.of(context).textTheme.headline3,)
          ),
          PopupMenuItem(
            value: "2",
            child: Text("report", style: Theme.of(context).textTheme.headline3,)
          ),
          PopupMenuItem(
            value: "2",
            child: Text("Fetish", style: Theme.of(context).textTheme.headline3,)
          ),
        ];
      },
    );
  }
}