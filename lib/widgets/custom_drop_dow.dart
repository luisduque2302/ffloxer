import 'package:flutter/material.dart';

class CustomDropDown extends StatefulWidget {

  CustomDropDown({
    this.label,
    this.items,
    this.function,
  });

  final String label;
  final void Function(String item) function;
  final List items;

  @override
  _CustomDropDownState createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  String selected;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 5,),
        Text(widget.label, style: Theme.of(context).textTheme.bodyText1,),
        DropdownButton(
          dropdownColor: Colors.black,
          items: widget.items.map((item) => DropdownMenuItem(
            child: Text(item),
          ),).toList(),
          onChanged: (value){
            setState(() {
              print("cambio");
              selected = value;
              widget.function(value);
            });
          },
          value: selected,
          style: Theme.of(context).textTheme.headline4,
        ),
      ],
    );
  }
}