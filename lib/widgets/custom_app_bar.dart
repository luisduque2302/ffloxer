import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'custom_popup_bottom_menu_filter.dart';

class CustomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      color: Theme.of(context).primaryColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            children: [
              CustomPopupBottomMenuFilter(),
              GestureDetector(
                onTap: (){
                  Navigator.pushNamed(context, "search");
                },
                child: FaIcon(FontAwesomeIcons.dotCircle, color: Colors.white, size: 20)
              )
            ],
          ),
          Text("Who's Nearby", style: Theme.of(context).textTheme.headline2,),
          IconButton(
            onPressed: (){},
            icon: FaIcon(FontAwesomeIcons.bars, color: Colors.white),
          ),
        ],
      ),
    );
  }
}