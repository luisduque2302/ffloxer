import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {

  CustomTextField({
    this.label,
    this.function,
    this.maxLines=1,
  });

  final String label;
  final void Function(String item) function;
  final maxLines;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextField(
        maxLines: maxLines,
        onChanged: function,
        decoration: InputDecoration(
          labelStyle: Theme.of(context).textTheme.bodyText1,
          labelText: label,
          enabledBorder: UnderlineInputBorder(      
            borderSide: BorderSide(color: Theme.of(context).primaryColor),   
          ),  
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Theme.of(context).primaryColor),
          ),
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: Theme.of(context).primaryColor),
          ),
        ),
        style: Theme.of(context).textTheme.headline4,
      ),
    );
  }
}