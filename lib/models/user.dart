import 'package:flutter/material.dart';

class User {

  User({
    this.userName,
    this.created_at
  });

  String userName;
  String created_at;
}